from django.conf import settings
from diario_extviews import settings as local_settings
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save
from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.base import RedirectView
from django import forms
from django.contrib import messages

from diario.models.resolver import DIARIO_ENTRY
from diario_moderation.models import (EntryStatus, STATUS_ACCEPTED, 
                                     STATUS_DRAFT, STATUS_QUEUED, 
                                     STATUS_REJECTED, STATUS_REWRITE,
                                     create_status_model)                                     
from diario.views.entries import EntryList


class ListByStatus(EntryList):
    '''Semi-generic view that return entry list depend on it's status'''
    queryset = None
    status = None
    
    def get_queryset(self):
        return DIARIO_ENTRY.objects.filter(entrystatus__status=self.status,
                                    author=self.request.user)

class ListDrafts(ListByStatus):
    '''Return a list of current user's drafts'''
    status = STATUS_DRAFT    
    template_name = 'diario/list_drafts.html'
    allow_empty = True
    

class ListQueued(ListByStatus):
    '''Return a list of current user's queued entries'''
    status = STATUS_QUEUED
    template_name = 'diario/list_queued.html'

class ListRejected(ListByStatus):
    '''Return a list of current user's rejected entries'''
    status = STATUS_REJECTED
    template_name = 'diario/list_rejected.html'

class ListRewrite(ListByStatus):
    '''Return a lisf of current user's entries that must be rewrited'''
    status = STATUS_REWRITE
    template_name = 'diario/list_rewrite.html'
 
 
class EntryCreationForm(forms.ModelForm):
    class Meta:
        model = DIARIO_ENTRY
        fields = ('title', 'body')
    

class CreateDraft(CreateView):
    model = DIARIO_ENTRY
    form_class = EntryCreationForm
    template_name = 'diario/create_draft.html'
    limit_exceed_template = 'limit_exceeded.html'
    context_object_name = 'form'
    
    def dispatch(self, request, *args, **kwargs):
        if not local_settings.DIARIO_LIMIT_DRAFTS or request.user.is_superuser:
            return super(CreateDraft, self).dispatch(request, *args, **kwargs)
            
        num_drafts = DIARIO_ENTRY.objects.filter(entrystatus__status=STATUS_DRAFT, author=request.user).count()
        if num_drafts >= local_settings.DIARIO_MAX_DRAFTS:
            return render(request, self.limit_exceed_template, {'limit': local_settings.DIARIO_MAX_DRAFTS })
        return super(CreateDraft, self).dispatch(request, *args, **kwargs)
    
    def form_valid(self, form):                
        instance = form.save(commit=False)
        instance.author = self.request.user
        instance.save()
        return super(CreateDraft, self).form_valid(form)
        
    def get_initial(self):
        initial = super(CreateDraft, self).get_initial()
        initial['author'] = self.request.user        
        return initial
        
    def get_success_url(self):
        return reverse('edit-draft', kwargs={'pk': self.object.pk})



class EditDraft(UpdateView):
    model = DIARIO_ENTRY
    form_class = EntryCreationForm
    template_name = 'diario/edit_draft.html'
    context_object_name = 'entry'
    queryset = None            

    def get_success_url(self):
        return reverse('edit-draft', kwargs={'pk': self.object.pk})   
        
    def get_queryset(self):       
        user = self.request.user
        queryset = DIARIO_ENTRY.objects.filter(entrystatus__status=STATUS_DRAFT)        
        if user.has_perm('diario_moderation.moderate_entry'):
            return queryset                
        return queryset.filter(author=user)
  

class QueueDraft(UpdateView):
    model = DIARIO_ENTRY
    queryset = EntryStatus.objects.filter(status=STATUS_DRAFT)  
    template_name = 'diario/edit_draft.html'    
    
    def get_success_url(self):
        #return reverse('list-queued')
        return '/' # stub
    
    def get_queryset(self):        
        return DIARIO_ENTRY.objects.filter(entrystatus__status=STATUS_DRAFT,
                                                author=self.request.user)
        
    def form_valid(self, form):                
        status = EntryStatus.objects.get(entry__pk=form.instance.pk)
        status.status = STATUS_QUEUED
        status.save()
        return super(QueueDraft, self).form_valid(form)
   
post_save.connect(create_status_model, sender=DIARIO_ENTRY)