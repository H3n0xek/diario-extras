from django import template
from diario.models.resolver import DIARIO_ENTRY
from diario_moderation.models import (STATUS_DRAFT, STATUS_ACCEPTED,
                                    STATUS_QUEUED, STATUS_REWRITE)

register = template.Library()                                    


class GetEntriesList(template.Node):
    def __init__(self, status, user, var):
        self._status = status
        self._user = template.Variable(user)
        self._var = var
    
    def render(self, context):
        try:
            user = self._user.resolve(context)            
            context[self._var] = DIARIO_ENTRY.objects.filter(entrystatus__status=self._status)
            print DIARIO_ENTRY.objects.filter(entrystatus__status=self._status)
        except template.VariableDoesNotExist:
            return ''
        return ''

@register.tag(name="get_draft_list")            
def do_get_draft_list(parser, token):
    try:
        tag_name, user, var = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires exactly two arguments" % token.contents.split()[0])
    return GetEntriesList(STATUS_DRAFT, user, var)

@register.tag(name="get_queued_list")
def do_get_queued_list(parser, token):
    try:
        tag_name, user, var = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires exactly two arguments" % token.contents.split()[0])
    return GetEntriesList(STATUS_QUEUED, user, var)

@register.tag(name="get_published_list")
def do_get_published_list(parser, token):
    try:
        tag_name, user, var = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires exactly two arguments" % token.contents.split()[0])
    return GetEntriesList(STATUS_ACCEPTED, user, var)
    
@register.tag(name="get_rewrite_list")
def do_get_rewrite_list(parser, token):
    try:
        tag_name, user, var = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires exactly two arguments" % token.contents.split()[0])
    return GetEntriesList(STATUS_REWRITE, user, var)    
    
