from django.contrib import admin
from diario_moderation.models import EntryStatus

class EntryStatusAdmin(admin.ModelAdmin):
    pass
    
admin.site.register(EntryStatus, EntryStatusAdmin)